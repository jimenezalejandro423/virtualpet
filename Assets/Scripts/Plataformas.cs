using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataformas : MonoBehaviour
{
    public GameObject plataforma;
    public float distanciaY = 1f;
    public float tiempo = 0.5f;
    public GameObject[] puntos;
    public GameObject moneda;
    public float offsetMoneda = 0.5f;
    private int indiceAnterior;
    private bool primeraVez;
    private int contador;

    void Start()
    {
        indiceAnterior = -1;
        primeraVez = true;
        contador = 0;
        InvokeRepeating("GenerarPlataforma", tiempo, tiempo);
    }
    void GenerarPlataforma()
    {
        if (puntos.Length > 0)
        {
            int indice;
            if (primeraVez)
            {
                indice = UnityEngine.Random.Range(0, puntos.Length);
                primeraVez = false;
            }
            else
            {
                do
                {
                    indice = UnityEngine.Random.Range(0, puntos.Length);
                } while (indice == indiceAnterior || (indice == 0 && indiceAnterior == 4) || (indice == 4 && indiceAnterior == 0));
            }
            indiceAnterior = indice;
            Vector3 posicion = puntos[indice].transform.position;
            Instantiate(plataforma, posicion, Quaternion.identity);
            contador++;

            if (contador % 3 == 0)
            {
                Instantiate(moneda, posicion + Vector3.up * offsetMoneda, Quaternion.identity);
            }
            for (int i = 0; i < puntos.Length; i++)
            {
                puntos[i].transform.position += Vector3.up * distanciaY;
            }
        }
    }

}


