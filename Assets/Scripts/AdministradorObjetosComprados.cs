using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdministradorObjetosComprados : MonoBehaviour
{

    public List<string> objetosCompradosIDs = new List<string>();

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void AgregarObjetoComprado(string objetoID)
    {
        objetosCompradosIDs.Add(objetoID);
    }
}
