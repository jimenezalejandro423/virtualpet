using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambioSprite : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public Sprite nuevoSprite;
    public Sprite Sprite2;
    public float maxY;
    public float maxY2;

    public ParticleSystem particulas;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (transform.position.y > maxY)
        {
            spriteRenderer.sprite = nuevoSprite;
        }
        if (transform.position.y > maxY2)
        {
            spriteRenderer.sprite = Sprite2;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Muerte"))
        {
            Debug.Log("Colision");
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            particulas.Play();
        }
    }
}   
