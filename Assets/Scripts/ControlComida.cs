using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ControlComida : MonoBehaviour
{
    [SerializeField]
    private Inventario inventario;

    void Start()
    {
        ActivarElemento();
    }

    public void ActivarElemento()
    {
        PlantillaInformacionItem item = inventario.ObtenerElemento();
        if(item != null)
        {
            GameObject go = Instantiate(item.objetoAsociado);
            go.GetComponent<Dragand>().info = item;
            go.GetComponent<Dragand>().OnComido += HanComido;
        }
    }

    private void HanComido(GameObject go)
    {
        go.GetComponent<Dragand>().OnComido -= HanComido;
        inventario.quitarObjeto(go.GetComponent<Dragand>().info);
        ActivarElemento();
    }
}


