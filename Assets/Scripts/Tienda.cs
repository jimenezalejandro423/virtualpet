using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    [SerializeField] List<PlantillaInformacionItem> informacionItems;
    [SerializeField] GameObject plantillaObjetoTienda;
    [SerializeField] TextMeshProUGUI textoMonedasTotales;

    void Start()
    {
        int monedasTotales = PlayerPrefs.GetInt("monedasTotales", 0);
        textoMonedasTotales.text = monedasTotales.ToString();

        foreach (var item in informacionItems)
        {
            GameObject newItem = Instantiate(plantillaObjetoTienda, transform);
            newItem.GetComponent<PlantillaItemTienda>().Iniciar(item);
        }
    }

    void Update()
    {
        textoMonedasTotales.text = PlayerPrefs.GetInt("monedasTotales").ToString();
    }

}
