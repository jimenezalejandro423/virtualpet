using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour
{
    private Animator animator;

    public bool Comer = false;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Comer == true)
        {
            StartCoroutine(comiendo());
        }
    }

    IEnumerator comiendo()
    {
        animator.SetTrigger("Comer");
        yield return new WaitForSeconds(0.3f);
        Comer = false;
    }
}
