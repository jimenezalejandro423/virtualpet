using UnityEngine;

public class DestruirPlataformas : MonoBehaviour
{
    private Camera mainCamera;
    private float minY;

    void Start()
    {
        mainCamera = Camera.main;
        minY = mainCamera.transform.position.y - mainCamera.orthographicSize - 2f;
    }

    void Update()
    {
        if (transform.position.y < minY)
        {
            Destroy(gameObject);
        }
    }
}


