using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour
{
    public int cantidadDeMonedas = 1;
    public AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            audio.Play();
            Debug.Log("sonando");
            PlantillaItemTienda itemTienda = FindObjectOfType<PlantillaItemTienda>();
            if (itemTienda != null)
            {
                itemTienda.sumar(cantidadDeMonedas);
            }
            Destroy(gameObject);
        }
    }
}

