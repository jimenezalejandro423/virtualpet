using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MenuTienda", menuName = "Plantillas/InfoItemTienda")]
public class PlantillaInformacionItem : ScriptableObject
{
    public string id;
    public string titulo;
    public Sprite image;
    public int precio;
    public int comida;
    public bool comprado;
    public GameObject objetoAsociado;

    public void ActivarObjetoAsociado()
    {
        objetoAsociado.SetActive(true);
    }
}
