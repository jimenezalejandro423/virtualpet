using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hambre : MonoBehaviour
{
    public Image barraHambre;
    public float tiempoHambre = 1f;
    public float cantidadHambre = 0.01f;
    public Color fullColor = Color.green;
    public Color halfColor = Color.yellow;
    public Color lowColor = Color.red;
    private float hambre;

    private bool fueComidoRecientemente = false;
    private float tiempoUltimaComida;

    private void Awake()
    {
        hambre = PlayerPrefs.GetFloat("Hambre", 1f);
        ActualizarBarra();
    }

    void Start()
    {
        StartCoroutine(DisminuirHambre());
    }

    void Update()
    {
        if (hambre <= 0f)
        {
            Debug.Log("Game over");
            StopCoroutine(DisminuirHambre());
        }
    }

    IEnumerator DisminuirHambre()
    {
        while (true)
        {
            if (!fueComidoRecientemente)
            {
                hambre -= cantidadHambre;
                hambre = Mathf.Max(hambre, 0f);
                ActualizarBarra();
                PlayerPrefs.SetFloat("Hambre", hambre);
                PlayerPrefs.Save();
            }

            yield return new WaitForSeconds(tiempoHambre);
        }
    }

    void ActualizarBarra()
    {
        if (hambre > 0.5f)
        {
            barraHambre.color = fullColor;
        }
        else if (hambre > 0.25f)
        {
            barraHambre.color = halfColor;
        }
        else
        {
            barraHambre.color = lowColor;
        }

        barraHambre.rectTransform.localScale = new Vector3(1f, hambre, 1f);
    }

    public void Comer()
    {
        hambre += 0.2f;
        hambre = Mathf.Clamp01(hambre);
        ActualizarBarra();
        fueComidoRecientemente = true;
        tiempoUltimaComida = Time.time;
        StartCoroutine(EsperarParaRestablecerTiempoEspera());
    }

    private IEnumerator EsperarParaRestablecerTiempoEspera()
    {
        yield return new WaitForSeconds(tiempoHambre);
        fueComidoRecientemente = false;
    }

    public void RestablecerTiempoEspera()
    {
        fueComidoRecientemente = false;
    }
}
