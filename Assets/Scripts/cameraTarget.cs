using UnityEngine;

public class SeguirJugadorVertical : MonoBehaviour
{
    public Transform jugador;
    public float velocidadSeguimiento = 5f;

    private float alturaInicial;

    private void Start()
    {
        alturaInicial = transform.position.y;
    }

    private void Update()
    {
        if (jugador.position.y > transform.position.y)
        {
            Vector3 nuevaPosicion = transform.position;
            nuevaPosicion.y = Mathf.Lerp(transform.position.y, jugador.position.y, velocidadSeguimiento * Time.deltaTime);
            transform.position = nuevaPosicion;
        }
    }
}
