using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragand : MonoBehaviour
{
    private Vector3 PInicial;
    private bool moviendose = false;
    private Vector3 offset;

    public PlantillaInformacionItem info;
    public Anim anim;

    public delegate void Comido(GameObject go);
    public event Comido OnComido;

    private Hambre hambreScript;

    private void Start()
    {
        PInicial = transform.position;
        hambreScript = FindObjectOfType<Hambre>();
        anim = FindAnyObjectByType<Anim>();
    }

    private void OnMouseDown()
    {
        offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        moviendose = true;
    }

    private void OnMouseUp()
    {
        moviendose = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.5f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag("Boca"))
            {
                anim.Comer = true;
                if (hambreScript != null)
                {
                    hambreScript.Comer();
                }

                OnComido?.Invoke(gameObject);
                Destroy(gameObject);
                break;
            }
        }

        transform.position = PInicial;
    }

    private void Update()
    {
        if (moviendose)
        {
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
            transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);
        }
    }
}
