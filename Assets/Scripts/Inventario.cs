using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventario", menuName = "Plantillas/Inventario")]
public class Inventario : ScriptableObject
{
    public List<PlantillaInformacionItem> objetos = new List<PlantillaInformacionItem>();

    public void añadirObjeto(PlantillaInformacionItem element)
    {
        objetos.Add(element);
    }

    public void quitarObjeto(PlantillaInformacionItem element)
    {
        objetos.Remove(element);
    }

    public PlantillaInformacionItem ObtenerElemento()
    {
        if (objetos.Count > 0)
            return objetos[0];
        return null;
    }
}
