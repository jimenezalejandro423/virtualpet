using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PlantillaItemTienda : MonoBehaviour
{
    [SerializeField] List<PlantillaInformacionItem> informacionItems;
    public Image imagen;
    public TextMeshProUGUI textoPrecio;
    public TextMeshProUGUI titulo;
    public TextMeshProUGUI textoComida;
    public Button botonComprar;
    public ControlComida controlcomida;
    [SerializeField]
    private Inventario inventario;

    private PlantillaInformacionItem infoObjecto;

    public int sumaMoneda;
    int precio;
    public int monedaTotales;
    int comida;
    string objetoID;


    public void Iniciar(PlantillaInformacionItem item)
    {

        imagen.sprite = item.image;
        titulo.text = item.titulo;
        textoPrecio.text = item.precio.ToString();
        textoComida.text = item.comida.ToString();
        objetoID = item.id;
        infoObjecto = item;
    }

    void Start()
    {
        precio = int.Parse(textoPrecio.text);
        comida = int.Parse(textoComida.text);

        monedaTotales = PlayerPrefs.GetInt("monedasTotales", 0);
    }

    void Update()
    {
        monedaTotales = PlayerPrefs.GetInt("monedasTotales");
        if (precio > monedaTotales)
        {
            botonComprar.interactable = false;
        }
    }

    public void Comprar()
    {
        Debug.Log("Comprando " + titulo.text);
        if (precio <= monedaTotales)
        {
            monedaTotales -= precio;
            inventario.añadirObjeto(infoObjecto);
            PlayerPrefs.SetInt("monedasTotales", monedaTotales);

            AdministradorObjetosComprados manager = FindObjectOfType<AdministradorObjetosComprados>();

            if (manager != null)
            {
                manager.AgregarObjetoComprado(objetoID);
            }
        }
    }

    public void sumar(int cantidad)
    {
        monedaTotales += cantidad;
        PlayerPrefs.SetInt("monedasTotales", monedaTotales);
        PlayerPrefs.Save();
        Debug.Log("Se han sumado " + cantidad + " monedas. Nuevo total: " + monedaTotales);
    }

}
