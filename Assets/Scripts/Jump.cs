using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Jump : MonoBehaviour
{
    public float velocidad;
    public float jumpForce = 1000f;
    public float gravityScale = 10f;
    public float maxFallSpeed = 50f;
    private Rigidbody2D rb;
    private SpriteRenderer sp;
    private bool isJumping;

    private Accions movimiento;


    private void Awake()
    {
        movimiento = new Accions();
    }

    private void OnEnable()
    {
        movimiento.Enable();
    }

    private void OnDisable()
    {
        movimiento?.Disable();
    }



    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sp = GetComponent<SpriteRenderer>();
        isJumping = false;
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (isJumping && rb.velocity.y < -maxFallSpeed)
        {
            Vector2 newVelocity = new Vector2(rb.velocity.x, -maxFallSpeed);
            rb.velocity = newVelocity;
        }

        float movimientoH = movimiento.Movimiento.Horizontal.ReadValue<float>();
        rb.velocity = new Vector2(movimientoH * velocidad, rb.velocity.y);

        if (movimientoH > 0)
        {
            sp.flipX = false;
        }
        else if (movimientoH < 0)
        {
            sp.flipX = true;
        }

        Vector3 playerPos = transform.position;
        playerPos.x += movimientoH * velocidad * Time.deltaTime;
        playerPos.x = Mathf.Clamp(playerPos.x, -7.5f, 7.5f);

        transform.position = playerPos;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            isJumping = false;
            rb.gravityScale = 1;
            saltar();
        }
    }



    public void saltar()
    {
        isJumping = true;
        rb.AddForce(Vector2.up * jumpForce);
        rb.gravityScale = gravityScale;
    }
}

